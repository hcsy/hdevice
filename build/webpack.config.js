let path = require('path')
let webpack = require('webpack')
const TerserPlugin = require('terser-webpack-plugin');
let packageInfo = require('../package.json');

function resolve(dir) {
    return path.join(__dirname, '..', dir);
}

module.exports = {
    context: path.resolve(__dirname, '../'),
    entry: {
        'hdevice': ['./src/utils/hdevice.js']
    },
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: `[name].js`,
        library: 'SDK',
        libraryTarget: "umd"
    },
    plugins: [
        new webpack.BannerPlugin('hdevice-' + packageInfo.version + " " + new Date().toLocaleString()),
    ],
    //压缩混淆 js
    //压缩js
    optimization: {
        minimizer: [
            new TerserPlugin({
                cache: true,
                sourceMap: false, //是否含有map文件
                parallel: true
            })
        ]
    }
}