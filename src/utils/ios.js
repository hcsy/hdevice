/**ios 系统的判断 */
/**
 * https://joyqi.github.io/mobile-device-js/example.html
 */
class IosParser {
  constructor(ua) {
    this.ua = ua;
    this.devices = [
      ['a7', '640x1136', ['iPhone 5', 'iPhone 5s']],
      ['a7', '1536x2048', ['iPad Air', 'iPad Mini 2', 'iPad Mini 3']],
      ['a8', '640x1136', ['iPod touch (6th gen)']],
      ['a8', '750x1334', ['iPhone 6']],
      ['a8', '1242x2208', ['iPhone 6 Plus']],
      ['a8', '1536x2048', ['iPad Air 2', 'iPad Mini 4']],
      ['a9', '640x1136', ['iPhone SE']],
      ['a9', '750x1334', ['iPhone 6s']],
      ['a9', '1242x2208', ['iPhone 6s Plus']],
      ['a9x', '1536x2048', ['iPad Pro (1st gen 9.7-inch)']],
      ['a9x', '2048x2732', ['iPad Pro (1st gen 12.9-inch)']],
      ['a10', '750x1334', ['iPhone 7']],
      ['a10', '1242x2208', ['iPhone 7 Plus']],
      ['a10x', '1668x2224', ['iPad Pro (2th gen 10.5-inch)']],
      ['a10x', '2048x2732', ['iPad Pro (2th gen 12.9-inch)']],
      ['a11', '750x1334', ['iPhone 8']],
      ['a11', '1242x2208', ['iPhone 8 Plus']],
      ['a11', '1125x2436', ['iPhone X']],
      ['a12', '828x1792', ['iPhone Xr']],
      ['a12', '1125x2436', ['iPhone Xs']],
      ['a12', '1242x2688', ['iPhone Xs Max']],
      ['a12x', '1668x2388', ['iPad Pro (3rd gen 11-inch)']],
      ['a12x', '2048x2732', ['iPad Pro (3rd gen 12.9-inch)']]
    ];
  }
  /**
   * 获取设备型号
   */
  getDeviceType() {

  }
  /**
   * 获取分辨率
   */
  getResolution() {
    let ratio = window.devicePixelRatio || 1;
    return (Math.min(screen.width, screen.height) * ratio)
      + 'x' + (Math.max(screen.width, screen.height) * ratio);
  }
  /**
   * 获取GPU
   */
  getGlRenderer() {
    if (this.glRenderer == null) {
      let canvas = document.createElement('canvas');
      let gl = canvas.getContext('experimental-webgl');
      let debugInfo = gl.getExtension('WEBGL_debug_renderer_info');
      this.glRenderer = debugInfo == null ? 'unknown' : gl.getParameter(debugInfo.UNMASKED_RENDERER_WEBGL);
    }
  }
  /**
   * 获取手机型号
   */
  getModels() {
    if (this.models == null) {
      this.getGlRenderer()
      let gpu = this.glRenderer;
      let matches = gpu.match(/^apple\s+([_a-z0-9-]+)\s+gpu$/i);
      let res = this.getResolution();
      this.models = 'unknown';
      if (matches) {
        for (let i = 0; i < this.devices.length; i++) {
          let device = this.devices[i];
          if (matches[1].toLowerCase() == device[0]
            && res == device[1]) {
            this.models = (device[2].join("")).toLowerCase();
            break;
          }
        }
      }
      if (this.models === 'unknown') {
        //根据尺寸进行判断 苹果的型号
        let isIPhoneX = /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 3 && window.screen.width === 375 && window.screen.height === 812;
        if (isIPhoneX) { this.models = "iphonex" };
        // iPhone XS Max
        let isIPhoneXSMax = /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 3 && window.screen.width === 414 && window.screen.height === 896;
        if (isIPhoneXSMax) { this.models = "iphonexmax" };
        // iPhone XR
        let isIPhoneXR = /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 2 && window.screen.width === 414 && window.screen.height === 896;
        if (isIPhoneXR) { this.models = "iphonexr" };
        if (screen.height == 812 && screen.width == 375) {
          this.models = "iphonex";
        } else if (screen.height == 736 && screen.width == 414) {
          this.models = "iphone6/iphone7/iphone8";
        } else if (screen.height == 667 && screen.width == 375) {
          this.models = "iphone6/iphone7/iphone8";
        } else if (screen.height == 568 && screen.width == 320) {
          this.models = "iphone5/iphone5s";
        } else if (this.ua.match(/ipad/i) == "ipad") {
          this.models = "ipad";
        }else {
          this.models = "iphone4";
        }
      }
      return this.models;
    } else {
      return this.models;
    }
  }
}

export default IosParser